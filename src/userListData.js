const userList = [
  {
    id: 1,
    name: "Андрей",
    middleName: "Юрьевич",
    surName: "Архипов",
    phone: "89067085646",
    email: "test@test.com",
    birthday: "1990-12-14",
  },
  {
    id: 2,
    name: "Людмила",
    middleName: "Алексеевна",
    surName: "Кабанова",
    phone: "89067145236",
    email: "admin@admin.com",
    birthday: "1990-06-22",
  },
  {
    id: 3,
    name: "Иван",
    middleName: "Григорьевич",
    surName: "Соломин",
    phone: "89055478585",
    email: "test@test.com",
    birthday: "1990-06-01",
  },
  {
    id: 4,
    name: "Андрей",
    middleName: "Юрьевич",
    surName: "Архипов",
    phone: "89067085646",
    email: "test@test.com",
    birthday: "1990-12-14",
  },
  {
    id: 5,
    name: "Людмила",
    middleName: "Алексеевна",
    surName: "Кабанова",
    phone: "89067145236",
    email: "admin@admin.com",
    birthday: "1990-06-22",
  },
  {
    id: 6,
    name: "Иван",
    middleName: "Григорьевич",
    surName: "Соломин",
    phone: "89055478585",
    email: "test@test.com",
    birthday: "1990-06-01",
  },
];

export default userList;
